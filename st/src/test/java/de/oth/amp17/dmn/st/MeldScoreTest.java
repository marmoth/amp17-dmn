package de.oth.amp17.dmn.st;

import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class MeldScoreTest {

    @Test
    void meldScoreViaRest() {

        JsonObject payload = Json.createObjectBuilder()
                .add("variables", Json.createObjectBuilder()
                        .add("scr", doubleVar(4.0))
                        .add("bilirubin", doubleVar(1.5))
                        .add("inr", doubleVar(3.0))
                        .add("na", doubleVar(125))
                ).build();

        System.out.println("request payload =\n" + payload);
        System.out.println();

        Client client = ClientBuilder.newClient();

        JsonArray response = client.target("http://localhost:8080/engine-rest/engine/default/decision-definition/key/meld/evaluate")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(
                        Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE),
                        JsonArray.class
                );

        System.out.println("response =\n" + response);


    }

    private JsonObjectBuilder doubleVar(double value) {
        return Json.createObjectBuilder()
                .add("value", value)
                .add("type", "Double");
    }

}
