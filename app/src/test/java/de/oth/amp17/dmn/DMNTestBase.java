package de.oth.amp17.dmn;

import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.camunda.bpm.engine.variable.VariableMap;
import org.junit.jupiter.api.BeforeEach;

public class DMNTestBase {

    private final String resource;

    private DmnEngine dmnEngine;

    @BeforeEach
    void setupEngine() {
        dmnEngine = DmnEngineConfiguration
                .createDefaultDmnEngineConfiguration()
                .buildEngine();
    }

    protected DMNTestBase(String resource) {
        this.resource = resource;
    }

    protected DmnDecisionResult evaluate(String decisionKey, VariableMap variables) {
        DmnDecision decision = dmnEngine.parseDecision(
                decisionKey, getClass().getResourceAsStream(resource));
        return dmnEngine.evaluateDecision(decision, variables);
    }
}
