package de.oth.amp17.dmn;

import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SpigelmanTest extends DMNTestBase {

    private static final String KEY_SPIGELMAN_SCORE = "spigelmanScore";
    private static final String KEY_SPIGELMAN_KLASSIFIKATION = "spigelmanKlassifikation";

    public SpigelmanTest() {
        super("/spigelman.dmn");
    }

    @ParameterizedTest
    @DisplayName("Spiegelman Score")
    @CsvSource({
            "1, 1, tubulär, niedriggradig, 4",
            "1, 1, , niedriggradig, 3",
            "1, 1, tubulär, , 3",
            ", , tubulär, , 1",
            "1, 25, tubulär, niedriggradig, 6"
    })
    void score(
            Integer anzahlPolypen,
            Integer polypengröße,
            String histologie,
            String intraepithelialeNeoplasie,
            Integer expectedScore
    ) {

        DmnDecisionResult result = evaluate(KEY_SPIGELMAN_SCORE, toVariableMap(
                anzahlPolypen, polypengröße, histologie, intraepithelialeNeoplasie));

        assertEquals(expectedScore, result.getSingleEntry());
    }

    @ParameterizedTest
    @DisplayName("Spiegelman Klassifikation")
    @CsvSource({
            "1, 1, tubulär, niedriggradig, 1",
            ", , villös, , 1",
            ", , villös, hochgradig, 2",
            "1, 25, tubulär, niedriggradig, 2",
            "25, 15, villös, hochgradig, 4"
    })
    void classification(
            Integer anzahlPolypen,
            Integer polypengröße,
            String histologie,
            String intraepithelialeNeoplasie,
            Integer expectedStage
    ) {

        DmnDecisionResult result = evaluate(KEY_SPIGELMAN_KLASSIFIKATION, toVariableMap(
                anzahlPolypen, polypengröße, histologie, intraepithelialeNeoplasie));

        assertEquals(expectedStage, result.getSingleEntry());
    }

    private VariableMap toVariableMap(Integer anzahlPolypen, Integer polypengröße, String histologie, String intraepithelialeNeoplasie) {
        return Variables.createVariables()
                .putValue("anzahlPolypen", anzahlPolypen)
                .putValue("polypengröße", polypengröße)
                .putValue("histologie", histologie)
                .putValue("intraepithelialeNeoplasie", intraepithelialeNeoplasie);
    }

}
