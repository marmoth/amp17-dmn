package de.oth.amp17.dmn;

import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.dmn.engine.DmnDecisionResultEntries;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class BodyMassIndexTest extends DMNTestBase {

    protected BodyMassIndexTest() {
        super("/bmi.dmn");
    }

    @ParameterizedTest
    @DisplayName("BMI berechnen")
    @CsvSource({
            "1.65, 77, 28.3",
            "1.65, 120, 44.1",
            "1, 1, 1"
    })
    void calculate(Double bodyHeight, Double bodyWeight, Double expectedBMI) {

        DmnDecisionResult result = evaluate(
                "calculateBMI",
                toVariableMap(bodyHeight, bodyWeight));

        assertEquals(expectedBMI, result.getSingleEntry(), 0.1);

    }


    @ParameterizedTest
    @DisplayName("Körpergewicht klassifizieren")
    @CsvSource({
            "1.65, 77, Übergewicht, ",
            "1.65, 120, Adipositas, 3"
    })
    void classify(Double bodyHeight, Double bodyWeight, String expectedCategory, Integer exptectedAdiposityStage) {

        DmnDecisionResult result = evaluate(
                "classifyBodyWeight",
                toVariableMap(bodyHeight, bodyWeight));

        assertAll(
                () -> {
                    assertFalse(result.isEmpty());

                    DmnDecisionResultEntries entries = result.getSingleResult();

                    assertAll(
                            () -> assertEquals(expectedCategory, entries.getEntry("bodyWeightCategory")),
                            () -> {
                                if (exptectedAdiposityStage == null) {
                                    assertFalse(entries.getEntryMap().containsKey("adiposityStage"));
                                } else {
                                    assertEquals(exptectedAdiposityStage, entries.getEntry("adiposityStage"));
                                }
                            }
                    );
                }
        );

    }

    private VariableMap toVariableMap(Double bodyHeight, Double bodyWeight) {
        return Variables.createVariables()
                .putValue("bodyHeight", bodyHeight)
                .putValue("bodyWeight", bodyWeight);
    }
}
