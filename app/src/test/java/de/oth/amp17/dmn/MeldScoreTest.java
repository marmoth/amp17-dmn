package de.oth.amp17.dmn;

import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.test.DmnEngineRule;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.junit.Rule;
import org.junit.Test;

import java.io.InputStream;

public class MeldScoreTest {

    @Rule
    public DmnEngineRule dmnEngineRule = new DmnEngineRule();

    @Test
    public void calculate_meld() {
        DmnEngine dmnEngine = dmnEngineRule.getDmnEngine();
        // load DMN file
        InputStream inputStream = getClass().getResourceAsStream("/meld_calc.dmn");
        //create and add variables
        VariableMap variables = Variables.createVariables()
                .putValue("scr", 4.0)
                .putValue("bilirubin", 1.5)
                .putValue("inr", 3.0)
                .putValue("na", 125);

        DmnDecision decision = dmnEngine.parseDecision("meld", inputStream);
        DmnDecisionResult result = dmnEngine.evaluateDecision(decision, variables);
        result.forEach(each -> {
            System.out.println(each.getEntryMap());
        });
    }

    @Test
    public void decide_rezertifizierung() {
        DmnEngine dmnEngine = dmnEngineRule.getDmnEngine();
        // load DMN file
        InputStream inputStream = getClass().getResourceAsStream("/meld_rezertifizierung.dmn");
        //create and add variables
        VariableMap variables = Variables.createVariables()
                .putValue("meld", 36);

        DmnDecision decision = dmnEngine.parseDecision("rezertifizierung", inputStream);
        DmnDecisionTableResult result = dmnEngine.evaluateDecisionTable(decision, variables);
        result.forEach(each -> {
            System.out.println(each.getEntryMap());
        });
    }

}
